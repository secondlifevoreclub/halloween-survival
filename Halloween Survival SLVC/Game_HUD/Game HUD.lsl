// Game_HUD.Game HUD.lslp 
// 2018-09-10 22:21:01 - LSLForge (0.1.9.3) generated

integer listenHandler;

integer wsSetHashedNumber(string fHash){
  return ((integer)("0x" + llGetSubString(llSHA1String((((string)fHash) + "bghuiofsebuiofauahnbfnfauhipofabnuipojdjl")),0,7)));
}

wsSetListen(key fAgent){
  if (listenHandler) {
    llListenRemove(listenHandler);
    (listenHandler = 0);
  }
  (listenHandler = llListen(wsSetHashedNumber(((string)fAgent)),"","",""));
}

wsBootup(key fAgent){
  if (listenHandler) {
    llListenRemove(listenHandler);
    (listenHandler = 0);
  }
  (listenHandler = llListen(wsSetHashedNumber(((string)fAgent)),"","",""));
  llRequestExperiencePermissions(fAgent,"");
  llSetTimerEvent(60);
}

default {

    on_rez(integer start_param) {
    (listenHandler = llListen(start_param,"","",""));
  }

    
    listen(integer channel,string name,key id,string message) {
    list tStartParams = llParseString2List(message,["|"],[]);
    if ((llList2String(tStartParams,0) == "ATTACH")) {
      wsBootup(llList2Key(tStartParams,1));
    }
  }

    
    experience_permissions(key target_id) {
    llAttachToAvatarTemp(35);
    llSetTimerEvent(0);
    if ((llGetAttached() == 0)) {
      llInstantMessage(target_id,"Could not attach HUD, please contact Wolf Seisenbacher for support!");
      llDie();
    }
  }

    
    experience_permissions_denied(key id,integer reason) {
    llInstantMessage(id,("Experience permissions MUST be accepted to play this game!\nError associated with denial: " + llGetExperienceErrorMessage(reason)));
    llDie();
  }

    
    attach(key id) {
    if (id) {
      llSetTimerEvent(0);
      llSay(0,(llGetDisplayName(id) + " has attached the Game Hud!"));
      state running;
    }
    else  {
      llDie();
    }
  }

    
    timer() {
    llDie();
  }
}

state running {

	state_entry() {
    wsSetListen(llGetOwner());
  }
}
