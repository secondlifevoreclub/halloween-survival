$import Game_HUD.API.lslm ();

default 
{
    on_rez(integer start_param)
    {
    	listenHandler = llListen(start_param, "","","");
    }
    
    listen(integer channel, string name, key id, string message)
    {
    	list tStartParams = llParseString2List(message, ["|"], []);
    	//tStartParams: COMMAND|KEY
    	if(llList2String(tStartParams, 0) == "ATTACH")
    	{
    		wsBootup(llList2Key(tStartParams, 1));
    	}
    }
    
    experience_permissions(key target_id)
    {
    	//Experience permissions granted!
    	llAttachToAvatarTemp(ATTACH_HUD_CENTER_1);
    	llSetTimerEvent(0);
    	if(llGetAttached() == 0)
    	{
    		//Attachment failed! kill object to prevent litter
    		llInstantMessage(target_id, "Could not attach HUD, please contact Wolf Seisenbacher for support!");
    		llDie();
    	}
    }
    
    experience_permissions_denied(key id, integer reason)
    {
    	//Experience permissions denied!
    	llInstantMessage(id, "Experience permissions MUST be accepted to play this game!\nError associated with denial: " + llGetExperienceErrorMessage(reason));
    	llDie();
    }
    
    attach(key id)
    {
    	if(id)
    	{
    		llSetTimerEvent(0);
    		llSay(0, llGetDisplayName(id) + " has attached the Game Hud!");
    		state running;
    	}
    	else
    	{
    		llDie();
    	}
    }
    
    timer()
    {
    	llDie();
    }
}

state running
{
	state_entry()
	{
		wsSetListen(llGetOwner());
	}
}