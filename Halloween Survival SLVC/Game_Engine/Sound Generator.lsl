// Game_Engine.Sound Generator.lslp 
// 2018-10-02 20:30:12 - LSLForge (0.1.9.3) generated
//Sound Generator Functions:
//Trigger type: Single, Loop, Loop for time, Stop
//Sounds List: Sound UUIDs loaded in Notecard Configuration
//UUID configuration has "Reference" names, for trigger.
//Configuration Type: Trigger or Ambient
//Trigger listens for game engine triggers
//Ambient plays constant, timed loops of sound(s)


string gGameIdentification = "SLVC Horror Survival";
integer gListenChannel;
key gGameEngineUUID;
list gIgnore = [];
list gSounds = [];
list gPlayTimes = [];
integer gType = -1;
float gUpdateInterval;
float gLoopTime = 0;
integer gTriggerType = 0;
string gNodeName;
list gConfigurationOutput;
integer gNotecardLine;
key gNotecardQuery;

wsPingHandler(string fIdentifier,string fModule,key fGameEngineID,integer fChannel){
  if (((fIdentifier == gGameIdentification) && 1)) {
    llRegionSayTo(fGameEngineID,fChannel,("MODULE_PING|RETURN|" + fModule));
    (gListenChannel = fChannel);
    (gGameEngineUUID = fGameEngineID);
  }
}


//Notecard Config Settings:
//TYPE: Ambient or Trigger
//Diverge config paths for each type.  if TYPE = NOT_SET throw error
//AMBIENT SETTINGS:
//LIST CONFIG: Sound UUID, Loop Timer, Volume
//TRIGGER SETTINGS:
//LIST CONFIG: Sound UUID, Reference Name
//TRIGGER TYPE DEFAULT: Loop, Loop4Time, Single
//Trigger Loop Time Default

integer wsSetConfigurationSetting(list fConfig){
  string fTest = llToUpper(llList2String(fConfig,0));
  if (((fTest != "TYPE") && (gType == -1))) {
    return -1;
  }
  else  if ((fTest == "TYPE")) {
    if ((llList2String(fConfig,1) == "AMBIENT")) {
      (gType = 1);
      return 1;
    }
    else  if ((llList2String(fConfig,1) == "TRIGGER")) {
      (gType = 0);
      return 1;
    }
    else  {
      return 0;
    }
  }
  else  if ((fTest == "NODE NAME")) {
    (gNodeName = llList2String(fConfig,1));
    if ((gNodeName == "")) {
      return 0;
    }
    else  {
      return 1;
    }
  }
  else  {
    if ((!gType)) {
      if ((fTest == "SOUND")) {
        list tSoundChecker = llCSV2List(llList2String(fConfig,1));
        if ((llList2Key(tSoundChecker,0) == NULL_KEY)) {
          return 0;
        }
        else  if ((llList2String(tSoundChecker,1) == "")) {
          return 0;
        }
        else  {
          (gSounds += tSoundChecker);
          return 1;
        }
      }
      else  if ((fTest == "TRIGGER TYPE")) {
        if ((llList2String(fConfig,1) == "LOOP")) {
          (gTriggerType = 0);
          return 1;
        }
        else  if ((llList2String(fConfig,1) == "LOOP FOR TIME")) {
          (gTriggerType = 1);
          return 1;
        }
        else  if ((llList2String(fConfig,1) == "SINGLE")) {
          (gTriggerType = 2);
          return 1;
        }
      }
      else  if ((fTest == "LOOP TIME")) {
        (gLoopTime = llList2Integer(fConfig,1));
        if ((gLoopTime <= 0)) {
          return 0;
        }
      }
    }
    else  if (gType) {
      if ((fTest == "SOUND")) {
        list tSoundChecker = llCSV2List(llList2String(fConfig,1));
        if ((llList2Key(tSoundChecker,0) == NULL_KEY)) {
          return 0;
        }
        else  if ((llList2Float(tSoundChecker,1) < 0)) {
          return 0;
        }
        else  if (((llList2Float(tSoundChecker,2) <= 0) || (llList2Float(tSoundChecker,2) > 1))) {
          return 0;
        }
        else  {
          (gSounds += tSoundChecker);
          return 1;
        }
      }
    }
  }
  return 0;
}

wsStartLoopedSounds(){
  list tTimes = llList2ListStrided(gSounds,2,-1,3);
  (tTimes = llListSort(tTimes,0,0));
  integer i;
  (gUpdateInterval = 0);
  for ((i = 0); (i < (llGetListLength(tTimes) - 1)); (++i));
  {
    float tUpdateTest = (llList2Float(tTimes,i) - llList2Float(tTimes,(i + 1)));
    if ((gUpdateInterval == 0)) {
      (gUpdateInterval = tUpdateTest);
    }
    else  if ((tUpdateTest < gUpdateInterval)) {
      (gUpdateInterval = tUpdateTest);
    }
  }
  integer tCurrentTime = llGetUnixTime();
  (tTimes = llList2ListStrided(gSounds,2,-1,3));
  for ((i = 0); (i < llGetListLength(tTimes)); (++i)) {
    (gPlayTimes = llListReplaceList(tTimes,[(llList2Integer(tTimes,i) + tCurrentTime)],i,i));
  }
  for ((i = 0); (i < llGetListLength(gPlayTimes)); (++i)) {
    if ((llList2Integer(gPlayTimes,i) == tCurrentTime)) {
      llLoopSound(llList2Key(gSounds,(i * 3)),llList2Float(gSounds,((i * 3) + 2)));
      (gPlayTimes = llListReplaceList(gPlayTimes,[-1],i,i));
    }
  }
  llSetTimerEvent(gUpdateInterval);
}

wsPlayLoopedSounds(){
  integer tCurrentTime = llGetUnixTime();
  integer i;
  for ((i = 0); (i < llGetListLength(gPlayTimes)); (++i)) {
    if ((llList2Integer(gPlayTimes,i) <= tCurrentTime)) {
      llPlaySound(llList2Key(gSounds,(i * 3)),llList2Float(gSounds,((i * 3) + 2)));
      (gPlayTimes = llListReplaceList(gPlayTimes,[(tCurrentTime + llList2Integer(gSounds,((i * 3) + 1)))],i,i));
    }
  }
}

wsAdjustTimer(){
  list tTimes = llListSort(gPlayTimes,0,1);
  float tUpdateInterval = (llList2Float(tTimes,0) - llGetUnixTime());
  if ((gUpdateInterval > tUpdateInterval)) {
    (gUpdateInterval = tUpdateInterval);
    llSetTimerEvent(gUpdateInterval);
  }
}

wsStopLoopedSounds(){
  llStopSound();
  llSetTimerEvent(0);
  (gPlayTimes = []);
  (gUpdateInterval = 0);
}

wsPlaySound(string fName,integer fType,float fTime,float fVolume){
  integer i = llListFindList(gSounds,[fName]);
  if ((i == -1)) {
    llRegionSay(2147483647,"ERROR: REFERENCE NAME NOT FOUND");
  }
  else  {
    if ((fType == 0)) {
      llLoopSound(llList2String(gSounds,(i - 1)),fVolume);
    }
    else  if ((fType == 1)) {
      llLoopSound(llList2String(gSounds,(i - 1)),fVolume);
      llSetTimerEvent(fTime);
    }
    else  if ((fType == 2)) {
      llPlaySound(llList2String(gSounds,(i - 1)),fVolume);
    }
  }
}

default {

    state_entry() {
    llListen(-7843275,"","","");
    llSetText("Listening for Game Engine",<1.0,1.0,1.0>,1);
  }

    
    listen(integer channel,string name,key id,string message) {
    if (((channel == -7843275) && (llListFindList(gIgnore,[id]) == -1))) {
      list tSystemCall = llParseString2List(message,["|"],[]);
      if ((llList2String(tSystemCall,0) == "MODULE_PING")) {
        wsPingHandler(llList2String(tSystemCall,1),"SOUNDS",id,llList2Integer(tSystemCall,2));
      }
      else  if ((message == "TURN_ON_MODULE")) {
        llSetText("Module Booting...",<1.0,1.0,1.0>,1);
        state boot;
      }
      else  if ((message == "IGNORE_PING_60")) {
        (gIgnore += [id,(llGetUnixTime() + 60)]);
        llSetTimerEvent(5);
      }
    }
  }

    
    timer() {
    if (llGetListLength(gIgnore)) {
      integer i;
      for ((i = llGetListLength(gIgnore)); (i > 0); (i -= 2)) {
        if ((llList2Integer(gIgnore,i) <= llGetUnixTime())) {
          (gIgnore = llDeleteSubList(gIgnore,(i - 1),i));
        }
      }
      if ((gIgnore == [])) {
        llSetTimerEvent(0);
      }
    }
  }
}

state boot {

	state_entry() {
    if ((llGetInventoryKey("Sounds.cfg") != NULL_KEY)) {
      (gConfigurationOutput = []);
      (gNotecardLine = 0);
      (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
    }
    else  {
      llSetText("ERROR: NO CONFIGURATION NOTECARD",<1.0,0.0,0.0>,1);
    }
  }

	
	dataserver(key query_id,string data) {
    if ((query_id == gNotecardQuery)) {
      if ((data == EOF)) {
        llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_LOADED|SOUNDS|" + gNodeName));
      }
      else  {
        (data = llStringTrim(data,3));
        if ((data != "")) {
          if ((llSubStringIndex(data,"//") != 0)) {
            if ((llSubStringIndex(data,"//") != -1)) {
              (data = llDeleteSubString(data,llSubStringIndex(data,"//"),-1));
            }
            list tData = llParseString2List(data,[":","="],[]);
            integer i;
            for ((i = 0); (i < llGetListLength(tData)); (++i)) {
              (tData = llListReplaceList(tData,[llStringTrim(llList2String(tData,i),3)],i,i));
            }
            (i = wsSetConfigurationSetting(tData));
            if (i) {
              (++gNotecardLine);
              (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
            }
            else  if ((i == -1)) {
              llSetText("ERROR: NO TYPE SET YET, CHECK CONFIGURATION",<1.0,0.0,0.0>,1);
            }
            else  {
              llSetText(("ERROR: INVALID CONFIGURATION\nLINE: " + ((string)gNotecardLine)),<1.0,0.0,0.0>,1);
            }
          }
          else  {
            (++gNotecardLine);
            (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
          }
        }
        else  {
          (++gNotecardLine);
          (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
        }
      }
    }
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "ACCEPT_MODULE")) {
      state running;
    }
    else  if ((message == "DUPLICATE_NODE")) {
      llSetText("ERROR: DUPLICATE NODE NAME",<1.0,0.0,0.0>,1);
    }
  }
}

state running {

	state_entry() {
    llListen(gListenChannel,"",gGameEngineUUID,"");
    llSetText("Awaitng Game to Start...",<1.0,1.0,1.0>,1);
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "START_GAME")) {
      llSetText("",<1.0,1.0,1.0>,1);
      if (gType) {
        state Trigger;
      }
      else  if ((!gType)) {
        state Ambient;
      }
    }
    else  if ((message == "RESET_GAME")) {
      llResetScript();
    }
    else  if ((message == "DUPLICATE_NODE")) {
      llSetText("ERROR: DUPLICATE NODE NAME",<1.0,0.0,0.0>,1);
    }
    else  if ((message == "RELOAD_CONFIGURATION")) {
      if ((llGetInventoryKey("Sounds.cfg") != NULL_KEY)) {
        llSetText("Loading Config...",<1.0,1.0,1.0>,1);
        (gConfigurationOutput = []);
        (gNotecardLine = 0);
        (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
      }
      else  {
        llSetText("ERROR: NO CONFIGURATION NOTECARD",<1.0,0.0,0.0>,1);
      }
    }
  }

	
	dataserver(key query_id,string data) {
    if ((query_id == gNotecardQuery)) {
      if ((data == EOF)) {
        llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_LOADED|SOUNDS|" + gNodeName));
      }
      else  {
        (data = llStringTrim(data,3));
        if ((data != "")) {
          if ((llSubStringIndex(data,"//") != 0)) {
            if ((llSubStringIndex(data,"//") != -1)) {
              (data = llDeleteSubString(data,llSubStringIndex(data,"//"),-1));
            }
            list tData = llParseString2List(data,[":","="],[]);
            integer i;
            for ((i = 0); (i < llGetListLength(tData)); (++i)) {
              (tData = llListReplaceList(tData,[llStringTrim(llList2String(tData,i),3)],i,i));
            }
            (i = wsSetConfigurationSetting(tData));
            if (i) {
              (++gNotecardLine);
              (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
            }
            else  if ((i == -1)) {
              llSetText("ERROR: NO TYPE SET YET, CHECK CONFIGURATION",<1.0,0.0,0.0>,1);
            }
            else  {
              llSetText(("ERROR: INVALID CONFIGURATION\nLINE: " + ((string)gNotecardLine)),<1.0,0.0,0.0>,1);
            }
          }
          else  {
            (++gNotecardLine);
            (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
          }
        }
        else  {
          (++gNotecardLine);
          (gNotecardQuery = llGetNotecardLine("Sounds.cfg",gNotecardLine));
        }
      }
    }
  }
}

state Trigger {

	state_entry() {
    llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_ACTIVE|SOUNDS|" + gNodeName));
    llListen(gListenChannel,"",gGameEngineUUID,"");
  }

	
	listen(integer channel,string name,key id,string message) {
    (message = llToUpper(message));
    if ((message == "NEW_GAME")) {
      llStopSound();
      state running;
    }
    else  if ((message == "SET_INACTIVE")) {
      llStopSound();
      state inactive;
    }
    else  {
      list tCommand = llParseString2List(message,["|"],[]);
      if ((llList2String(tCommand,0) == "PLAY")) {
        string tRef = ((string)llListFindList(tCommand,["REF"]));
        float tVol = ((float)llListFindList(tCommand,["VOL"]));
        float tTim = ((float)llListFindList(tCommand,["TIM"]));
        integer tTyp = llListFindList(tCommand,["TYP"]);
        if ((tVol != -1)) {
          (tVol = llList2Float(tCommand,((integer)tVol)));
        }
        else  {
          (tVol = 0.5);
        }
        if ((tTim != -1)) {
          (tTim = llList2Float(tCommand,((integer)tTim)));
        }
        else  {
          (tTim = gLoopTime);
        }
        if ((tTyp != -1)) {
          (tTyp = llList2Integer(tCommand,tTyp));
        }
        else  {
          (tTyp = gType);
        }
        if ((tRef != "-1")) {
          (tRef = llList2String(tCommand,(((integer)tRef) + 1)));
          wsPlaySound(tRef,tTyp,tTim,tVol);
        }
        else  {
          llRegionSay(2147483647,"ERROR: REFERENCE NAME NOT GIVEN");
        }
      }
      if ((llList2String(tCommand,0) == "STOP")) {
        llStopSound();
        llSetTimerEvent(0);
      }
    }
  }

	
	timer() {
    llStopSound();
    llSetTimerEvent(0);
  }
}

state Ambient {

	state_entry() {
    llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_ACTIVE|SOUNDS|" + gNodeName));
    llListen(gListenChannel,"",gGameEngineUUID,"");
    wsStartLoopedSounds();
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "NEW_GAME")) {
      wsStopLoopedSounds();
      state running;
    }
    else  if ((message == "SET_INACTIVE")) {
      wsStopLoopedSounds();
      state inactive;
    }
  }

	
	timer() {
    wsPlayLoopedSounds();
    wsAdjustTimer();
  }
}


state inactive {

	state_entry() {
    llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_INACTIVE|SOUNDS|" + gNodeName));
    llListen(gListenChannel,"",gGameEngineUUID,"");
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "NEW_GAME")) {
      state running;
    }
    else  if ((message == "SET_ACTIVE")) {
      if (gType) {
        state Trigger;
      }
      else  if ((!gType)) {
        state Ambient;
      }
    }
  }
}
