// Game_Engine.Core Engine.lslp 
// 2018-09-11 19:58:24 - LSLForge (0.1.9.3) generated

list gEngineModules = ["CLUE_PAGE","CRATE","HIDING_SPOT","SOUND_GENERATOR","TRAP","EVENT_TRIGGER","MAP_GENERATOR","ANTAGONIST"];
integer gAdminHudChannel = -7654326;
string gGameIdentification = "SLVC Horror Survival";
integer gListenChannel;
integer gHandler;
list gLoadedModules = [];
integer gModuleFound = 0;

integer wsListenChannel(key fHash){
  return ((integer)("0x" + llGetSubString(llSHA1String((((string)fHash) + "bjuiofsweuibohanio;gfajikgasnjkl;airnbhui")),0,7)));
}

wsInitializeSettings(){
  {
    {
      (gAdminHudChannel = 2177245660);
    }
    llListen(gAdminHudChannel,"","","");
  }
}

default {

    state_entry() {
    llOwnerSay((gGameIdentification + " bootup phase initiating..."));
    (gListenChannel = wsListenChannel(llGetKey()));
    (gHandler = llListen(gListenChannel,"","",""));
    wsInitializeSettings();
    {
      llOwnerSay("Calling out for modules within simulator");
      llRegionSay(-7843275,((("MODULE_PING|" + gGameIdentification) + "|") + ((string)gListenChannel)));
      llSetTimerEvent(15);
    }
  }

    
    listen(integer channel,string name,key id,string message) {
    if ((channel == gAdminHudChannel)) {
    }
    else  if ((channel == gListenChannel)) {
      if ((llGetSubString(message,0,17) == "MODULE_PING|RETURN")) {
        string tModuleName = llStringTrim(name,3);
        string tModuleType = llDeleteSubString(message,0,18);
        integer tModuleEnabled = llListFindList(gEngineModules,[tModuleType]);
        if ((tModuleEnabled != -1)) {
          (gLoadedModules += [tModuleName,tModuleType]);
          llRegionSayTo(id,-7843275,"TURN_ON_MODULE");
          (gModuleFound = 1);
        }
        else  {
          llRegionSayTo(id,-7843275,"IGNORE_PINGS_60");
        }
      }
    }
  }

    
    timer() {
    if (gModuleFound) {
      (gModuleFound = 0);
      llOwnerSay("Modules found, pinging for more!");
      llRegionSay(-7843275,("MODULE_PING|" + ((string)gListenChannel)));
    }
    else  {
      llOwnerSay("No more modules returned a ping.  Moving on!");
      llOwnerSay("Modules Loaded:");
      integer i;
      string tMessage;
      for ((i = 0); (i < llGetListLength(gLoadedModules)); (i += 2)) {
        (tMessage += (((llList2String(gLoadedModules,i) + ", of Type: ") + llList2String(gLoadedModules,(i + 1))) + "\n"));
      }
      (tMessage = llDeleteSubString(tMessage,-1,-1));
      if ((tMessage == "")) {
        llOwnerSay("No Modules Loaded!");
      }
      else  {
        llOwnerSay(tMessage);
      }
      llSetTimerEvent(0);
    }
  }
}
