$module ()

//Sound Settings
list gSounds = [];
list gPlayTimes = [];
integer gType = -1; //-1 = NOT SET, 0 = Trigger, 1 = Ambient.
float gUpdateInterval;
float gLoopTime = 0;
integer gTriggerType = 0; //0 = Loop, 1 = Loop4Time, 2 = Single.
string gNodeName;

//Notecard Loading configuration
list gConfigurationOutput;
integer gNotecardLine;
key gNotecardQuery;


//Notecard Config Settings:
//TYPE: Ambient or Trigger
//Diverge config paths for each type.  if TYPE = NOT_SET throw error
//AMBIENT SETTINGS:
//LIST CONFIG: Sound UUID, Loop Timer, Volume
//TRIGGER SETTINGS:
//LIST CONFIG: Sound UUID, Reference Name
//TRIGGER TYPE DEFAULT: Loop, Loop4Time, Single
//Trigger Loop Time Default

integer wsSetConfigurationSetting(list fConfig)
{
	string fTest = llToUpper(llList2String(fConfig, 0));
	if(fTest != "TYPE" && gType == -1)
	{
		return -1; //-1 = No Type Set
	}
	else if(fTest == "TYPE")
	{
		if(llList2String(fConfig, 1) == "AMBIENT")
		{
			gType = 1;
			return 1;
		}
		else if(llList2String(fConfig, 1) == "TRIGGER")
		{
			gType = 0;
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else if(fTest == "NODE NAME")
	{
		gNodeName = llList2String(fConfig, 1);
		if(gNodeName == "")
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		if(!gType)
		{
			if(fTest == "SOUND")
			{
				list tSoundChecker = llCSV2List(llList2String(fConfig, 1));
				if(llList2Key(tSoundChecker, 0) == NULL_KEY)
				{
					return 0;
				}
				else if(llList2String(tSoundChecker, 1) == "")
				{
					return 0;
				}
				else
				{
					gSounds += tSoundChecker;
					return 1;
				}
			}
			else if(fTest == "TRIGGER TYPE")
			{
				if(llList2String(fConfig, 1) == "LOOP")
				{
					gTriggerType = 0;
					return 1;
				}
				else if(llList2String(fConfig, 1) == "LOOP FOR TIME")
				{
					gTriggerType = 1;
					return 1;
				}
				else if(llList2String(fConfig, 1) == "SINGLE")
				{
					gTriggerType = 2;
					return 1;
				}
			}
			else if(fTest == "LOOP TIME")
			{
				gLoopTime = llList2Integer(fConfig, 1);
				if(gLoopTime <= 0)
				{
					return 0;
				}
			}
		}
		else if(gType)
		{
			if(fTest == "SOUND")
			{
				list tSoundChecker = llCSV2List(llList2String(fConfig, 1));
				if(llList2Key(tSoundChecker, 0) == NULL_KEY)
				{
					return 0;
				}
				else if(llList2Float(tSoundChecker, 1) < 0)
				{
					return 0;
				}
				else if(llList2Float(tSoundChecker, 2) <= 0 || llList2Float(tSoundChecker, 2) > 1)
				{
					return 0;
				}
				else
				{
					gSounds += tSoundChecker;
					return 1;
				}
			}
		}
	}
	return 0;
}

wsStartLoopedSounds()
{
	//STEPS
	//Get all Loop Times, find the shortest
	//Set timer to the shortest difference
	//Set correlate "Next Play" to unix time
	//For all sounds with 0 time, play constant loop.
	list tTimes = llList2ListStrided(gSounds, 2, -1, 3);
	tTimes = llListSort(tTimes, 0, FALSE);
	//Get shortest time for timer event refresh
	integer i;
	gUpdateInterval = 0;
	for(i=0;i<(llGetListLength(tTimes)-1);++i);
	{
		float tUpdateTest = llList2Float(tTimes, i) - llList2Float(tTimes, i+1);
		if(gUpdateInterval == 0)
		{
			gUpdateInterval = tUpdateTest;
		}
		else if(tUpdateTest<gUpdateInterval)
		{
			gUpdateInterval = tUpdateTest;
		}
	}
	//Get Unix Times, for when the sounds will play next
	integer tCurrentTime = llGetUnixTime();
	tTimes = llList2ListStrided(gSounds, 2, -1, 3);
	for(i=0;i<llGetListLength(tTimes);++i)
	{
		gPlayTimes = llListReplaceList(tTimes, [llList2Integer(tTimes,i)+tCurrentTime], i, i);
	}
	//Play all constant looping sounds in list
	for(i=0; i<llGetListLength(gPlayTimes); ++i)
	{
		if(llList2Integer(gPlayTimes, i) == tCurrentTime)
		{
			llLoopSound(llList2Key(gSounds, i*3), llList2Float(gSounds, (i*3)+2));
			gPlayTimes = llListReplaceList(gPlayTimes, [-1], i, i);  //Set next play time to constant looping ones to -1 to denote always playing.
		}
	}
	//All set! lets start the timer event!
	llSetTimerEvent(gUpdateInterval);
}

wsPlayLoopedSounds()
{
	integer tCurrentTime = llGetUnixTime();
	integer i;
	//Go though list of gPlayTimes, find which ones are due for play, and play sounds @ volume.
	for(i=0; i<llGetListLength(gPlayTimes); ++i)
	{
		if(llList2Integer(gPlayTimes, i) <= tCurrentTime)
		{
			llPlaySound(llList2Key(gSounds, i*3), llList2Float(gSounds, (i*3)+2));
			gPlayTimes = llListReplaceList(gPlayTimes, [tCurrentTime + llList2Integer(gSounds, (i*3)+1)], i, i);
		}
	}
}

wsAdjustTimer()
{
	//Compare next time withe update interval.  if next time is due before the next update, reset timer to that new time.
	list tTimes = llListSort(gPlayTimes, 0, TRUE);
	float tUpdateInterval = llList2Float(tTimes, 0) - llGetUnixTime();
	if(gUpdateInterval>tUpdateInterval)
	{
		gUpdateInterval = tUpdateInterval;
		llSetTimerEvent(gUpdateInterval);
	}
}

wsStopLoopedSounds()
{
	llStopSound();
	llSetTimerEvent(0);
	gPlayTimes = [];
	gUpdateInterval = 0;
}

wsPlaySound(string fName, integer fType, float fTime, float fVolume)
{
	integer i = llListFindList(gSounds, [fName]);
	if(i==-1)
	{
		llRegionSay(DEBUG_CHANNEL, "ERROR: REFERENCE NAME NOT FOUND");
	}
	else
	{
		if(fType == 0) //loop
		{
			llLoopSound(llList2String(gSounds, i-1), fVolume);
		}
		else if(fType == 1) //Loop for Time
		{
			llLoopSound(llList2String(gSounds, i-1), fVolume);
			llSetTimerEvent(fTime);
		}
		else if(fType == 2) // Single Play
		{
			llPlaySound(llList2String(gSounds, i-1), fVolume);
		}
	}
}

