$import Game_Engine.EngineSettings.lslm ();
$import Game_Engine.EngineAPI.lslm ();
$import Game_Engine.Core_Engine_Functions.lslm;

default 
{
    state_entry() 
    {
        llOwnerSay(gGameIdentification + " bootup phase initiating...");
        gListenChannel = wsListenChannel(llGetKey());
        gHandler = llListen(gListenChannel, "","","");
        wsInitializeSettings();
        if(gQuickBoot)
        {
        	llOwnerSay("Calling out for modules within simulator");
        	llRegionSay(gBootupChannel, "MODULE_PING|" + gGameIdentification + "|" + (string)gListenChannel); //Ping all active modules in region.  Let modules know this engine's communication channel.
        	llSetTimerEvent(15); //Set Timeout for module ping.
        }
        else
        {
        	llOwnerSay("System ready for initialization instructions");
        }
    }
    
    listen(integer channel, string name, key id, string message)
    {
    	if(channel == gAdminHudChannel)
    	{
    		//Admin HUD command code!
    	}
    	else if(channel == gListenChannel)
    	{
    		if(llGetSubString(message, 0, 17) == "MODULE_PING|RETURN")
    		{
    			string tModuleName = llStringTrim(name, STRING_TRIM); //Use object name as module name!
    			string tModuleType = llDeleteSubString(message, 0, 18); //Get Module Type, from end of return ping.
    			integer tModuleEnabled = llListFindList(gEngineModules, [tModuleType]);
    			if(tModuleEnabled != -1)
    			{
    				gLoadedModules += [tModuleName,tModuleType]; //Add module name and type to loaded modules list.
    				llRegionSayTo(id, gBootupChannel, "TURN_ON_MODULE"); //Let the module know to flip to the next state.
    				gModuleFound = TRUE;
    			}
    			else
    			{
    				llRegionSayTo(id, gBootupChannel, "IGNORE_PINGS_60"); //Tell the module to ignore any more pings from this engine for 60s
    			}
    		}
    	}
    }
    
    timer()
    {
    	if(gModuleFound)
    	{
    		gModuleFound = FALSE;
    		llOwnerSay("Modules found, pinging for more!");
    		llRegionSay(gBootupChannel, "MODULE_PING|" + (string)gListenChannel);
    	}
    	else
    	{
    		llOwnerSay("No more modules returned a ping.  Moving on!");
    		llOwnerSay("Modules Loaded:");
    		integer i;
    		string tMessage;
    		for(i=0;i<llGetListLength(gLoadedModules);i+=2)
    		{
    			tMessage += llList2String(gLoadedModules,i) + ", of Type: " + llList2String(gLoadedModules, i+1) + "\n";
    		}
    		tMessage = llDeleteSubString(tMessage, -1, -1);
    		if(tMessage == "")
    		{
    			llOwnerSay("No Modules Loaded!");
    		}
    		else
    		{
    			llOwnerSay(tMessage);
    		}
    		llSetTimerEvent(0);
    	}
    }
}

