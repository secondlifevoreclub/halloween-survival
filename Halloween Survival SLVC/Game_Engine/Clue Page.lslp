$import Game_Engine.EngineAPI.lslm;
$import Game_Engine.Clue_Functions.lslm;

default 
{
    state_entry() 
    {
        llListen(gBootupChannel, "","",""); //Set the listen for bootup commands
        llSetText("Listening for Game Engine", <1,1,1>,1); //Floating text for knowing what's going on.
    }
    
    listen(integer channel, string name, key id, string message)
    {
    	if(channel == gBootupChannel && llListFindList(gIgnore, [id]) == -1)
    	{
    		list tSystemCall = llParseString2List(message, ["|"],[]);
    		if(llList2String(tSystemCall, 0) == "MODULE_PING")
    		{
    			wsPingHandler(llList2String(tSystemCall, 1), "CLUE_PAGE", id, llList2Integer(tSystemCall, 2));
    		}
    		else if(message == "TURN_ON_MODULE")
    		{
    			llSetText("Module Booting...",<1,1,1>,1);
    			//Set State to a bootup state!
    			state boot;
    		}
    		else if(message == "IGNORE_PING_60")
    		{
    			gIgnore += [id, llGetUnixTime()+60];
    			llSetTimerEvent(5);
    		}
    	}
    } 
    
    timer()
    {
    	//If gIgnore has something in it, check the Unix Timeout times for each item.  if passed timeout time, remove the ignored system.
    	if(llGetListLength(gIgnore))
    	{
    		integer i;
    		for(i=llGetListLength(gIgnore);i>0;i-=2)
    		{
    			if(llList2Integer(gIgnore, i) <= llGetUnixTime())
    			{
    				gIgnore = llDeleteSubList(gIgnore, i-1, i);
    			}
    		}
    		if(gIgnore == [])
    		{
    			llSetTimerEvent(0);
    		}
    	}
    }
}

state boot
{
	state_entry()
	{
		//Bootup requirements:
		//Load configuration Notecard.
		//Set page #, -1 is Superflous
		//Set pick-up narration text
		//Set page texture UUID
		//Set Point Value
		//Set Pickup Distance
		if(llGetInventoryKey("Clue_Page.cfg") != NULL_KEY)
		{
			gConfigurationOutput = [];
			gNotecardLine = 0;
			gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
		}
		else
		{
			llSetText("ERROR: NO CONFIGURATION NOTECARD", <1,0,0>, 1);
		}
	}
	
	dataserver(key query_id, string data)
	{
		if(query_id == gNotecardQuery)
		{
			if(data==EOF)
			{
				//DONE
				llSetTexture(gCluePageTex, ALL_SIDES);
				llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_LOADED|CLUE_PAGE|" + (string)gPageNum); //Let the game engine know CLUE_PAGE module is loaded, with gPageNum.
			}
			else
			{
				//Parse data to a list, and trim any whitespace.  Also, trim any comments!
				data = llStringTrim(data, STRING_TRIM);
				if(data != "")
				{
					if(llSubStringIndex(data, "//") != 0)
					{
						if(llSubStringIndex(data, "//") != -1)
						{
							data = llDeleteSubString(data, llSubStringIndex(data, "//"), -1);
						}
						list tData = llParseString2List(data, [":", "="], []);
						integer i;
						for(i=0; i<llGetListLength(tData);++i)
						{
							tData = llListReplaceList(tData, [llStringTrim(llList2String(tData, i), STRING_TRIM)], i, i);
						}
						i = wsSetConfigurationSetting(tData); //Reuse i variable for checking if configuration set properly
						if(i)
						{
							//If so, move on
							++gNotecardLine;
							gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
						}
						else
						{
							//If an invalid line, call an error and halt.
							llSetText("ERROR: INVALID CONFIGURATION\nLINE: " + (string)gNotecardLine, <1,0,0>,1);
						}
					}
					else
					{
						++gNotecardLine;
						gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
					}
					
				}
				else
				{
					++gNotecardLine;
					gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
				}
			}
		}
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "ACCEPT_MODULE")
		{
			state running;
		}
		else if(message == "PAGE_NUM_OCCUPIED")
		{
			llSetText("ERROR: PAGE NUMBER IN USE.",<1,0,0>,1);
		}
	}
								
}

//Running State is awaiting game to start.

state running
{
	state_entry()
	{
		llListen(gListenChannel, "",gGameEngineUUID, "");
		llSetText("Awaitng Game to Start...", <1,1,1>,1);
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "START_GAME")
		{
			llSetText("",<1,1,1>,1);
			state active;
		}
		else if(message == "RESET_GAME")
		{
			llResetScript();
		}
		else if(message == "RELOAD_CONFIGURATION")
		{
			if(llGetInventoryKey("Clue_Page.cfg") != NULL_KEY)
			{
				llSetText("Loading Config...", <1,1,1>, 1);
				gConfigurationOutput = [];
				gNotecardLine = 0;
				gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
			}
			else
			{
				llSetText("ERROR: NO CONFIGURATION NOTECARD", <1,0,0>, 1);
			}
		}
	}
	
	dataserver(key query_id, string data)
	{
		if(query_id == gNotecardQuery)
		{
			if(data==EOF)
			{
				//DONE
				llSetText("Awaitng Game to Start...", <1,1,1>,1);
				llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_LOADED|CLUE_PAGE|" + (string)gPageNum); //Let the game engine know CLUE_PAGE module is loaded, with gPageNum.
				llSetTexture(gCluePageTex, ALL_SIDES);
			}
			else
			{
				//Parse data to a list, and trim any whitespace.  Also, trim any comments!
				data = llStringTrim(data, STRING_TRIM);
				if(data != "")
				{
					if(llSubStringIndex(data, "//") != 0)
					{
						if(llSubStringIndex(data, "//") != -1)
						{
							data = llDeleteSubString(data, llSubStringIndex(data, "//"), -1);
						}
						list tData = llParseString2List(data, [":", "="], []);
						integer i;
						for(i=0; i<llGetListLength(tData);++i)
						{
							tData = llListReplaceList(tData, [llStringTrim(llList2String(tData, i), STRING_TRIM)], i, i);
						}
						i = wsSetConfigurationSetting(tData); //Reuse i variable for checking if configuration set properly
						if(i)
						{
							//If so, move on
							++gNotecardLine;
							gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
						}
						else
						{
							//If an invalid line, call an error and halt.
							llSetText("ERROR: INVALID CONFIGURATION\nLINE: " + (string)gNotecardLine, <1,0,0>,1);
						}
					}
					else
					{
						++gNotecardLine;
						gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
					}
					
				}
				else
				{
					++gNotecardLine;
					gNotecardQuery = llGetNotecardLine("Clue_Page.cfg", gNotecardLine);
				}
			}
		}
	}
	
}

//Active state is for when game is running, and clue page is not found yet.
state active
{
	//FUNCTION
	//Agent clicks
	//Page checks distance
	//If distance okay, Page pings HUD
	//Hud replies
	//Page calls to game engine for page give
	//Game engine communicates to HUD the update
	//Game engine tells page to go invisible
	//Page goes invisible, plays narration.
	state_entry()
	{
		llSetAlpha(1,ALL_SIDES);
		llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_ACTIVE|CLUE_PAGE|" + (string)gPageNum); //Let the engine know the module has activated successfully.
		llListen(gListenChannel, "",gGameEngineUUID, "");
	}
	
	touch_start(integer num_detected)
	{
		if(num_detected>1)
		{
			//Detect range, closest gets page.
			llSensor("","",AGENT, (float)gPickupDist, PI); //Detect ALL agents within gPickupDist.
		}
		else
		{
			//Detect range.
			llSensor("",llDetectedKey(0), AGENT, (float)gPickupDist, PI); //Detect only the specific agent within gPickupDist.
		}
	}
	sensor(integer detected)
	{
		if(detected > 1)
		{
			integer i; 
			list tDistances;
			for(i=0; i<=detected; ++i)
			{
				tDistances += [llRound(llVecDist(llDetectedPos(i),llGetPos())), llDetectedKey(i)];
			}
			tDistances = llListSort(tDistances, 2, TRUE);
			if(llList2Integer(tDistances, 0) == llList2Integer(tDistances, 2))
			{
				//Fumble for the page text goes here, no one gets page
				llSay(0, "TEMPORARY CHAT! YOU FUMBLE FOR PAGES! OH CLUTZ!");
			}
			else
			{
				key tWinner = llList2Key(tDistances, 1);
				if(tWinner == "NULL_KEY")
				{
					//ERROR
				}
				else
				{
					//Call for HUD ping
					if(gHandler)
					{
						llListenRemove(gHandler);
					}
					gHandler = llListen(gListenChannel+1, "","","");
					llWhisper(gListenChannel+1, "PING_HUD|" + (string)tWinner);
				}
			}	
		}
		else
		{
			//No one else... and within the distance? Okay, lets just ping.
			if(gHandler)
			{
				llListenRemove(gHandler);
			}
			gHandler = llListen(gListenChannel+1, "","","");
			llWhisper(gListenChannel+1, "PING_HUD|" + (string)llDetectedKey(0));
		}
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(id == gGameEngineUUID) //Game engine replies go here
		{
			if(message == "PAGE_GIVE_SUCCESSFUL"||
			message == "SET_INACTIVE")
			{
				llSetAlpha(0, ALL_SIDES);
				state inactive;
			}
			if(message == "NEW_GAME")
			{
				llSetAlpha(1, ALL_SIDES);
				state running;
			}
		}
		else //HUD based communications here.
		{
			//We got the page ping from the HUD
			if(message == "PING_PAGE")
			{
				llListenRemove(gHandler);
				gHandler = 0;
				llRegionSayTo(gGameEngineUUID, gListenChannel, "GIVE_PAGE|" + (string)id + "|" + (string)gPoints + "|" + (string)gCluePageTex + "|" + (string)gPageNum); //GIVE_PAGE|HUD UUID|POINTS|TEXTURE UUID|PAGE_NUM
			}
		}
	}
}

//Inactive state is for when game is running, and clue page is found.
state inactive
{
	state_entry()
	{
		llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_INACTIVE|CLUE_PAGE|" + (string)gPageNum);
		llListen(gListenChannel, "",gGameEngineUUID, ""); //This state ONLY listens for Game Engine communications.
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "NEW_GAME")
		{
			llSetAlpha(1, ALL_SIDES);
			state running;
		}
		else if(message == "SET_ACTIVE")
		{
			llSetAlpha(1, ALL_SIDES);
			state active;
		}
	}
}
