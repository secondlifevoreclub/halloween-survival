// Game_Engine.Clue Page.lslp 
// 2018-09-20 20:00:23 - LSLForge (0.1.9.3) generated

string gGameIdentification = "SLVC Horror Survival";
integer gListenChannel;
integer gHandler;
key gGameEngineUUID;
list gIgnore = [];
integer gPageNum;
integer gPickupDist;
integer gPoints;
string gNarration;
string gCluePageTex;
list gConfigurationOutput;
integer gNotecardLine;
key gNotecardQuery;

wsPingHandler(string fIdentifier,string fModule,key fGameEngineID,integer fChannel){
  if (((fIdentifier == gGameIdentification) && 1)) {
    llRegionSayTo(fGameEngineID,fChannel,("MODULE_PING|RETURN|" + fModule));
    (gListenChannel = fChannel);
    (gGameEngineUUID = fGameEngineID);
  }
}

integer wsSetConfigurationSetting(list fConfig){
  string fTest = llToUpper(llList2String(fConfig,0));
  if ((fTest == "GPAGENUM")) {
    (gPageNum = llList2Integer(fConfig,1));
    if ((gPageNum < -1)) {
      return 0;
    }
    else  {
      return 1;
    }
  }
  else  if ((fTest == "GPICKUPDIST")) {
    (gPickupDist = llList2Integer(fConfig,1));
    return 1;
  }
  else  if ((fTest == "GPOINTS")) {
    (gPoints = llList2Integer(fConfig,1));
    return 1;
  }
  else  if ((fTest == "GNARRATION")) {
    (gNarration = llList2String(fConfig,1));
    return 1;
  }
  else  if ((fTest == "GCLUEPAGETEX")) {
    (gCluePageTex = llList2String(fConfig,1));
    return 1;
  }
  return 0;
}

default {

    state_entry() {
    llListen(-7843275,"","","");
    llSetText("Listening for Game Engine",<1.0,1.0,1.0>,1);
  }

    
    listen(integer channel,string name,key id,string message) {
    if (((channel == -7843275) && (llListFindList(gIgnore,[id]) == -1))) {
      list tSystemCall = llParseString2List(message,["|"],[]);
      if ((llList2String(tSystemCall,0) == "MODULE_PING")) {
        wsPingHandler(llList2String(tSystemCall,1),"CLUE_PAGE",id,llList2Integer(tSystemCall,2));
      }
      else  if ((message == "TURN_ON_MODULE")) {
        llSetText("Module Booting...",<1.0,1.0,1.0>,1);
        state boot;
      }
      else  if ((message == "IGNORE_PING_60")) {
        (gIgnore += [id,(llGetUnixTime() + 60)]);
        llSetTimerEvent(5);
      }
    }
  }

    
    timer() {
    if (llGetListLength(gIgnore)) {
      integer i;
      for ((i = llGetListLength(gIgnore)); (i > 0); (i -= 2)) {
        if ((llList2Integer(gIgnore,i) <= llGetUnixTime())) {
          (gIgnore = llDeleteSubList(gIgnore,(i - 1),i));
        }
      }
      if ((gIgnore == [])) {
        llSetTimerEvent(0);
      }
    }
  }
}

state boot {

	state_entry() {
    if ((llGetInventoryKey("Clue_Page.cfg") != NULL_KEY)) {
      (gConfigurationOutput = []);
      (gNotecardLine = 0);
      (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
    }
    else  {
      llSetText("ERROR: NO CONFIGURATION NOTECARD",<1.0,0.0,0.0>,1);
    }
  }

	
	dataserver(key query_id,string data) {
    if ((query_id == gNotecardQuery)) {
      if ((data == EOF)) {
        llSetTexture(gCluePageTex,-1);
        llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_LOADED|CLUE_PAGE|" + ((string)gPageNum)));
      }
      else  {
        (data = llStringTrim(data,3));
        if ((data != "")) {
          if ((llSubStringIndex(data,"//") != 0)) {
            if ((llSubStringIndex(data,"//") != -1)) {
              (data = llDeleteSubString(data,llSubStringIndex(data,"//"),-1));
            }
            list tData = llParseString2List(data,[":","="],[]);
            integer i;
            for ((i = 0); (i < llGetListLength(tData)); (++i)) {
              (tData = llListReplaceList(tData,[llStringTrim(llList2String(tData,i),3)],i,i));
            }
            (i = wsSetConfigurationSetting(tData));
            if (i) {
              (++gNotecardLine);
              (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
            }
            else  {
              llSetText(("ERROR: INVALID CONFIGURATION\nLINE: " + ((string)gNotecardLine)),<1.0,0.0,0.0>,1);
            }
          }
          else  {
            (++gNotecardLine);
            (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
          }
        }
        else  {
          (++gNotecardLine);
          (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
        }
      }
    }
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "ACCEPT_MODULE")) {
      state running;
    }
    else  if ((message == "PAGE_NUM_OCCUPIED")) {
      llSetText("ERROR: PAGE NUMBER IN USE.",<1.0,0.0,0.0>,1);
    }
  }
}

//Running State is awaiting game to start.

state running {

	state_entry() {
    llListen(gListenChannel,"",gGameEngineUUID,"");
    llSetText("Awaitng Game to Start...",<1.0,1.0,1.0>,1);
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "START_GAME")) {
      llSetText("",<1.0,1.0,1.0>,1);
      state active;
    }
    else  if ((message == "RESET_GAME")) {
      llResetScript();
    }
    else  if ((message == "RELOAD_CONFIGURATION")) {
      if ((llGetInventoryKey("Clue_Page.cfg") != NULL_KEY)) {
        llSetText("Loading Config...",<1.0,1.0,1.0>,1);
        (gConfigurationOutput = []);
        (gNotecardLine = 0);
        (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
      }
      else  {
        llSetText("ERROR: NO CONFIGURATION NOTECARD",<1.0,0.0,0.0>,1);
      }
    }
  }

	
	dataserver(key query_id,string data) {
    if ((query_id == gNotecardQuery)) {
      if ((data == EOF)) {
        llSetText("Awaitng Game to Start...",<1.0,1.0,1.0>,1);
        llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_LOADED|CLUE_PAGE|" + ((string)gPageNum)));
        llSetTexture(gCluePageTex,-1);
      }
      else  {
        (data = llStringTrim(data,3));
        if ((data != "")) {
          if ((llSubStringIndex(data,"//") != 0)) {
            if ((llSubStringIndex(data,"//") != -1)) {
              (data = llDeleteSubString(data,llSubStringIndex(data,"//"),-1));
            }
            list tData = llParseString2List(data,[":","="],[]);
            integer i;
            for ((i = 0); (i < llGetListLength(tData)); (++i)) {
              (tData = llListReplaceList(tData,[llStringTrim(llList2String(tData,i),3)],i,i));
            }
            (i = wsSetConfigurationSetting(tData));
            if (i) {
              (++gNotecardLine);
              (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
            }
            else  {
              llSetText(("ERROR: INVALID CONFIGURATION\nLINE: " + ((string)gNotecardLine)),<1.0,0.0,0.0>,1);
            }
          }
          else  {
            (++gNotecardLine);
            (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
          }
        }
        else  {
          (++gNotecardLine);
          (gNotecardQuery = llGetNotecardLine("Clue_Page.cfg",gNotecardLine));
        }
      }
    }
  }
}

//Active state is for when game is running, and clue page is not found yet.
state active {

	//FUNCTION
	//Agent clicks
	//Page checks distance
	//If distance okay, Page pings HUD
	//Hud replies
	//Page calls to game engine for page give
	//Game engine communicates to HUD the update
	//Game engine tells page to go invisible
	//Page goes invisible, plays narration.
	state_entry() {
    llSetAlpha(1,-1);
    llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_ACTIVE|CLUE_PAGE|" + ((string)gPageNum)));
    llListen(gListenChannel,"",gGameEngineUUID,"");
  }

	
	touch_start(integer num_detected) {
    if ((num_detected > 1)) {
      llSensor("","",1,((float)gPickupDist),3.14159265);
    }
    else  {
      llSensor("",llDetectedKey(0),1,((float)gPickupDist),3.14159265);
    }
  }

	sensor(integer detected) {
    if ((detected > 1)) {
      integer i;
      list tDistances;
      for ((i = 0); (i <= detected); (++i)) {
        (tDistances += [llRound(llVecDist(llDetectedPos(i),llGetPos())),llDetectedKey(i)]);
      }
      (tDistances = llListSort(tDistances,2,1));
      if ((llList2Integer(tDistances,0) == llList2Integer(tDistances,2))) {
        llSay(0,"TEMPORARY CHAT! YOU FUMBLE FOR PAGES! OH CLUTZ!");
      }
      else  {
        key tWinner = llList2Key(tDistances,1);
        if ((tWinner == "NULL_KEY")) {
        }
        else  {
          if (gHandler) {
            llListenRemove(gHandler);
          }
          (gHandler = llListen((gListenChannel + 1),"","",""));
          llWhisper((gListenChannel + 1),("PING_HUD|" + ((string)tWinner)));
        }
      }
    }
    else  {
      if (gHandler) {
        llListenRemove(gHandler);
      }
      (gHandler = llListen((gListenChannel + 1),"","",""));
      llWhisper((gListenChannel + 1),("PING_HUD|" + ((string)llDetectedKey(0))));
    }
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((id == gGameEngineUUID)) {
      if (((message == "PAGE_GIVE_SUCCESSFUL") || (message == "SET_INACTIVE"))) {
        llSetAlpha(0,-1);
        state inactive;
      }
      if ((message == "NEW_GAME")) {
        llSetAlpha(1,-1);
        state running;
      }
    }
    else  {
      if ((message == "PING_PAGE")) {
        llListenRemove(gHandler);
        (gHandler = 0);
        llRegionSayTo(gGameEngineUUID,gListenChannel,((((((("GIVE_PAGE|" + ((string)id)) + "|") + ((string)gPoints)) + "|") + ((string)gCluePageTex)) + "|") + ((string)gPageNum)));
      }
    }
  }
}

//Inactive state is for when game is running, and clue page is found.
state inactive {

	state_entry() {
    llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_INACTIVE|CLUE_PAGE|" + ((string)gPageNum)));
    llListen(gListenChannel,"",gGameEngineUUID,"");
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "NEW_GAME")) {
      llSetAlpha(1,-1);
      state running;
    }
    else  if ((message == "SET_ACTIVE")) {
      llSetAlpha(1,-1);
      state active;
    }
  }
}
