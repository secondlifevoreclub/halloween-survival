//Event Trigger System
//FUNCTION: Trigger special events.  This will just be a generic trigger.
//EXAMPLE: Triggering antagonist howl
//EXAMPLE: Triggering atmosphere change
//EXAMPLE: Triggering different scenery changes (Dead bodies appearing nearby/behind, more blood, rain effects, etc.

$import Game_Engine.EngineAPI.lslm;
$import Game_Engine.TriggerFunctions.lslm;


default 
{
    state_entry() 
    {
        llListen(gBootupChannel, "","",""); //Set the listen for bootup commands
        llSetText("Listening for Game Engine", <1,1,1>,1); //Floating text for knowing what's going on.
    }
    
    listen(integer channel, string name, key id, string message)
    {
    	if(channel == gBootupChannel && llListFindList(gIgnore, [id]) == -1)
    	{
    		list tSystemCall = llParseString2List(message, ["|"],[]);
    		if(llList2String(tSystemCall, 0) == "MODULE_PING")
    		{
    			wsPingHandler(llList2String(tSystemCall, 1), "TRIGGER", id, llList2Integer(tSystemCall, 2));
    		}
    		else if(message == "TURN_ON_MODULE")
    		{
    			llSetText("Module Booting...",<1,1,1>,1);
    			//Set State to a bootup state!
    			state boot;
    		}
    		else if(message == "IGNORE_PING_60")
    		{
    			gIgnore += [id, llGetUnixTime()+60];
    			llSetTimerEvent(5);
    		}
    	}
    } 
    
    timer()
    {
    	//If gIgnore has something in it, check the Unix Timeout times for each item.  if passed timeout time, remove the ignored system.
    	if(llGetListLength(gIgnore))
    	{
    		integer i;
    		for(i=llGetListLength(gIgnore);i>0;i-=2)
    		{
    			if(llList2Integer(gIgnore, i) <= llGetUnixTime())
    			{
    				gIgnore = llDeleteSubList(gIgnore, i-1, i);
    			}
    		}
    		if(gIgnore == [])
    		{
    			llSetTimerEvent(0);
    		}
    	}
    }
}

state boot
{
	state_entry()
	{
		if(llGetInventoryKey("Trigger.cfg") != NULL_KEY)
		{
			gConfigurationOutput = [];
			gNotecardLine = 0;
			gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
		}
		else
		{
			llSetText("ERROR: NO CONFIGURATION NOTECARD", <1,0,0>, 1);
		}
	}
	
	dataserver(key query_id, string data)
	{
		if(query_id == gNotecardQuery)
		{
			if(data==EOF)
			{
				//DONE
				llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_LOADED|TRIGGER|" + gEventName); 
			}
			else
			{
				//Parse data to a list, and trim any whitespace.  Also, trim any comments!
				data = llStringTrim(data, STRING_TRIM);
				if(data != "")
				{
					if(llSubStringIndex(data, "//") != 0)
					{
						if(llSubStringIndex(data, "//") != -1)
						{
							data = llDeleteSubString(data, llSubStringIndex(data, "//"), -1);
						}
						list tData = llParseString2List(data, [":", "="], []);
						integer i;
						for(i=0; i<llGetListLength(tData);++i)
						{
							tData = llListReplaceList(tData, [llStringTrim(llList2String(tData, i), STRING_TRIM)], i, i);
						}
						i = wsSetConfigurationSetting(tData); //Reuse i variable for checking if configuration set properly
						if(i)
						{
							//If so, move on
							++gNotecardLine;
							gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
						}
						else
						{
							//If an invalid line, call an error and halt.
							llSetText("ERROR: INVALID CONFIGURATION\nLINE: " + (string)gNotecardLine, <1,0,0>,1);
						}
					}
					else
					{
						++gNotecardLine;
						gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
					}
					
				}
				else
				{
					++gNotecardLine;
					gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
				}
			}
		}
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "ACCEPT_MODULE")
		{
			state running;
		}
		else if(message == "DUPLICATE_EVENT")
		{
			llSetText("ERROR: DUPLICATE EVENT NAME",<1,0,0>,1);
		}
	}
}

state running
{
	state_entry()
	{
		llListen(gListenChannel, "",gGameEngineUUID, "");
		llSetText("Awaitng Game to Start...", <1,1,1>,1);
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "START_GAME")
		{
			llSetText("",<1,1,1>,1);
			state active;
		}
		else if(message == "RESET_GAME")
		{
			llResetScript();
		}
		else if(message == "DUPLICATE_EVENT")
		{
			llSetText("ERROR: DUPLICATE EVENT NAME",<1,0,0>,1);
		}
		else if(message == "RELOAD_CONFIGURATION")
		{
			if(llGetInventoryKey("Trigger.cfg") != NULL_KEY)
			{
				llSetText("Loading Config...", <1,1,1>, 1);
				gConfigurationOutput = [];
				gNotecardLine = 0;
				gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
			}
			else
			{
				llSetText("ERROR: NO CONFIGURATION NOTECARD", <1,0,0>, 1);
			}
		}
	}
	
	dataserver(key query_id, string data)
	{
		if(query_id == gNotecardQuery)
		{
			if(data==EOF)
			{
				//DONE
				llSetText("Awaitng Game to Start...", <1,1,1>,1);
				llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_LOADED|TRIGGER|" + gEventName);
			}
			else
			{
				//Parse data to a list, and trim any whitespace.  Also, trim any comments!
				data = llStringTrim(data, STRING_TRIM);
				if(data != "")
				{
					if(llSubStringIndex(data, "//") != 0)
					{
						if(llSubStringIndex(data, "//") != -1)
						{
							data = llDeleteSubString(data, llSubStringIndex(data, "//"), -1);
						}
						list tData = llParseString2List(data, [":", "="], []);
						integer i;
						for(i=0; i<llGetListLength(tData);++i)
						{
							tData = llListReplaceList(tData, [llStringTrim(llList2String(tData, i), STRING_TRIM)], i, i);
						}
						i = wsSetConfigurationSetting(tData); //Reuse i variable for checking if configuration set properly
						if(i)
						{
							//If so, move on
							++gNotecardLine;
							gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
						}
						else
						{
							//If an invalid line, call an error and halt.
							llSetText("ERROR: INVALID CONFIGURATION\nLINE: " + (string)gNotecardLine, <1,0,0>,1);
						}
					}
					else
					{
						++gNotecardLine;
						gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
					}
					
				}
				else
				{
					++gNotecardLine;
					gNotecardQuery = llGetNotecardLine("Trigger.cfg", gNotecardLine);
				}
			}
		}
	}
}

state active
{
	state_entry()
	{
		llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_ACTIVE|TRIGGER|" + gEventName); //Let the engine know the module has activated successfully.
		llListen(gListenChannel, "",gGameEngineUUID, "");
		llVolumeDetect(TRUE);
	}
	
	collision_start(integer num_detected)
	{
		if(!gProcessing)
		{
			gProcessing = TRUE;
			gAgentTriggered = llDetectedKey(0);
			if(!gAllowNearby && !gEndTrigger) //If the event is not allowed nearby, and there is NO end trigger for this event
			{
				gSensorEnabled = TRUE;
				llSensorRepeat("","",AGENT,gRange, PI, 1); //Check for any agent nearby, within gRange, every second (tick).
			}
			else if(gAllowNearby)
			{
				if(gDelay == 0)
				{
					llRegionSayTo(gGameEngineUUID, gListenChannel, "EVENT_START|" + gEventName + "|" + llDumpList2String(gTriggerResults, "|"));
				}
				else
				{
					llSetTimerEvent(gDelay);
				}
			}
		}
	}
	
	collision_end(integer num_detected)
	{
		if(gAgentTriggered == llDetectedKey(0))
		{
			if(!gEndTrigger && gAllowNearby)
			{
				if(gEventStarted)
				{
					llRegionSayTo(gGameEngineUUID, gListenChannel, "EVENT_END|" + gEventName);
					gProcessing = FALSE;
					gAgentTriggered = NULL_KEY;
				}
				else if(gDelay) //If the person leaves the event area, before gDelay is over, cancel the event.
				{
					llSetTimerEvent(0);
				}
			}
			else if(gEndTrigger && !gAllowNearby)
			{
				if(gSensorEnabled)
				{
					llSensorRemove();
				}
				llSensorRepeat("","",AGENT,gRange, PI, 1); //Check for any agent nearby, within gRange, every second (tick).
			}
			else
			{
				if(gDelay == 0)
				{
					llRegionSayTo(gGameEngineUUID, gListenChannel, "EVENT_END|" + gEventName + "|" + llDumpList2String(gEndTriggerResults, "|"));
					gProcessing = FALSE;
					gAgentTriggered = NULL_KEY;
				}
				else
				{
					llSetTimerEvent(gDelay);
				}
			}
		}
	}
	
	sensor(integer num_detected)
	{
		if(!num_detected)
		{
			gSensorEnabled = FALSE;
			if(gDelay == 0)
			{
				llRegionSayTo(gGameEngineUUID, gListenChannel, "EVENT_START|" + gEventName + "|" + llDumpList2String(gTriggerResults, "|"));
			}
			else
			{
				llSetTimerEvent(gDelay);
			}
			llSensorRemove();
		}
	}
	
	timer()
	{
		llRegionSayTo(gGameEngineUUID, gListenChannel, "EVENT_START|" + gEventName + "|" + llDumpList2String(gTriggerResults, "|"));
		if(gAllowNearby)
		{
			gEventStarted = TRUE;
		}
		llSetTimerEvent(0);
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "NEW_GAME")
		{
			state running;
		}
		else if(message == "SET_INACTIVE")
		{
			if(gEventStarted && gEndTrigger)
			{
				llRegionSayTo(gGameEngineUUID, gListenChannel, "EVENT_END|" + gEventName + "|" + llDumpList2String(gEndTriggerResults, "|"));
			}
			gProcessing = FALSE;
			gAgentTriggered = NULL_KEY;
			state inactive;
		}
	}
}

state inactive
{
	state_entry()
	{
		llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_INACTIVE|TRIGGER|" + gEventName);
		llListen(gListenChannel, "",gGameEngineUUID, ""); //This state ONLY listens for Game Engine communications.
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "NEW_GAME")
		{
			state running;
		}
		else if(message == "SET_ACTIVE")
		{
			state active;
		}
	}
}
	