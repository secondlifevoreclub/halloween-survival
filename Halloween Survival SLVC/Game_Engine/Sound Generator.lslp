//Sound Generator Functions:
//Trigger type: Single, Loop, Loop for time, Stop
//Sounds List: Sound UUIDs loaded in Notecard Configuration
//UUID configuration has "Reference" names, for trigger.
//Configuration Type: Trigger or Ambient
//Trigger listens for game engine triggers
//Ambient plays constant, timed loops of sound(s)

//TO TRIGGER:
//Call: Reference Name, Volume, Type, and Loop Time.  Separate by |
//EXMAPLE CALL
//llRegionSayTo(<SND-GEN-ID>, gChannel, "PLAY|REF|HOWL|VOL|0.2|TYP|2|TIM|0");  //Order not important, as long as you call the right commands.

$import Game_Engine.EngineAPI.lslm;
$import Game_Engine.SoundFunctions.lslm;

default 
{
    state_entry() 
    {
        llListen(gBootupChannel, "","",""); //Set the listen for bootup commands
        llSetText("Listening for Game Engine", <1,1,1>,1); //Floating text for knowing what's going on.
    }
    
    listen(integer channel, string name, key id, string message)
    {
    	if(channel == gBootupChannel && llListFindList(gIgnore, [id]) == -1)
    	{
    		list tSystemCall = llParseString2List(message, ["|"],[]);
    		if(llList2String(tSystemCall, 0) == "MODULE_PING")
    		{
    			wsPingHandler(llList2String(tSystemCall, 1), "SOUNDS", id, llList2Integer(tSystemCall, 2));
    		}
    		else if(message == "TURN_ON_MODULE")
    		{
    			llSetText("Module Booting...",<1,1,1>,1);
    			//Set State to a bootup state!
    			state boot;
    		}
    		else if(message == "IGNORE_PING_60")
    		{
    			gIgnore += [id, llGetUnixTime()+60];
    			llSetTimerEvent(5);
    		}
    	}
    } 
    
    timer()
    {
    	//If gIgnore has something in it, check the Unix Timeout times for each item.  if passed timeout time, remove the ignored system.
    	if(llGetListLength(gIgnore))
    	{
    		integer i;
    		for(i=llGetListLength(gIgnore);i>0;i-=2)
    		{
    			if(llList2Integer(gIgnore, i) <= llGetUnixTime())
    			{
    				gIgnore = llDeleteSubList(gIgnore, i-1, i);
    			}
    		}
    		if(gIgnore == [])
    		{
    			llSetTimerEvent(0);
    		}
    	}
    }
}

state boot
{
	state_entry()
	{
		if(llGetInventoryKey("Sounds.cfg") != NULL_KEY)
		{
			gConfigurationOutput = [];
			gNotecardLine = 0;
			gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
		}
		else
		{
			llSetText("ERROR: NO CONFIGURATION NOTECARD", <1,0,0>, 1);
		}
	}
	
	dataserver(key query_id, string data)
	{
		if(query_id == gNotecardQuery)
		{
			if(data==EOF)
			{
				//DONE
				llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_LOADED|SOUNDS|" + gNodeName); 
			}
			else
			{
				//Parse data to a list, and trim any whitespace.  Also, trim any comments!
				data = llStringTrim(data, STRING_TRIM);
				if(data != "")
				{
					if(llSubStringIndex(data, "//") != 0)
					{
						if(llSubStringIndex(data, "//") != -1)
						{
							data = llDeleteSubString(data, llSubStringIndex(data, "//"), -1);
						}
						list tData = llParseString2List(data, [":", "="], []);
						integer i;
						for(i=0; i<llGetListLength(tData);++i)
						{
							tData = llListReplaceList(tData, [llStringTrim(llList2String(tData, i), STRING_TRIM)], i, i);
						}
						i = wsSetConfigurationSetting(tData); //Reuse i variable for checking if configuration set properly
						if(i)
						{
							//If so, move on
							++gNotecardLine;
							gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
						}
						else if(i == -1)
						{
							llSetText("ERROR: NO TYPE SET YET, CHECK CONFIGURATION", <1,0,0>,1);
						}
						else
						{
							//If an invalid line, call an error and halt.
							llSetText("ERROR: INVALID CONFIGURATION\nLINE: " + (string)gNotecardLine, <1,0,0>,1);
						}
					}
					else
					{
						++gNotecardLine;
						gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
					}
					
				}
				else
				{
					++gNotecardLine;
					gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
				}
			}
		}
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "ACCEPT_MODULE")
		{
			state running;
		}
		else if(message == "DUPLICATE_NODE")
		{
			llSetText("ERROR: DUPLICATE NODE NAME",<1,0,0>,1);
		}
	}
}

state running
{
	state_entry()
	{
		llListen(gListenChannel, "",gGameEngineUUID, "");
		llSetText("Awaitng Game to Start...", <1,1,1>,1);
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "START_GAME")
		{
			llSetText("",<1,1,1>,1);
			if(gType)
			{
				state Trigger;
			}
			else if(!gType)
			{
				state Ambient;
			}
		}
		else if(message == "RESET_GAME")
		{
			llResetScript();
		}
		else if(message == "DUPLICATE_NODE")
		{
			llSetText("ERROR: DUPLICATE NODE NAME",<1,0,0>,1);
		}
		else if(message == "RELOAD_CONFIGURATION")
		{
			if(llGetInventoryKey("Sounds.cfg") != NULL_KEY)
			{
				llSetText("Loading Config...", <1,1,1>, 1);
				gConfigurationOutput = [];
				gNotecardLine = 0;
				gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
			}
			else
			{
				llSetText("ERROR: NO CONFIGURATION NOTECARD", <1,0,0>, 1);
			}
		}
	}
	
	dataserver(key query_id, string data)
	{
		if(query_id == gNotecardQuery)
		{
			if(data==EOF)
			{
				//DONE
				llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_LOADED|SOUNDS|" + gNodeName); 
			}
			else
			{
				//Parse data to a list, and trim any whitespace.  Also, trim any comments!
				data = llStringTrim(data, STRING_TRIM);
				if(data != "")
				{
					if(llSubStringIndex(data, "//") != 0)
					{
						if(llSubStringIndex(data, "//") != -1)
						{
							data = llDeleteSubString(data, llSubStringIndex(data, "//"), -1);
						}
						list tData = llParseString2List(data, [":", "="], []);
						integer i;
						for(i=0; i<llGetListLength(tData);++i)
						{
							tData = llListReplaceList(tData, [llStringTrim(llList2String(tData, i), STRING_TRIM)], i, i);
						}
						i = wsSetConfigurationSetting(tData); //Reuse i variable for checking if configuration set properly
						if(i)
						{
							//If so, move on
							++gNotecardLine;
							gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
						}
						else if(i == -1)
						{
							llSetText("ERROR: NO TYPE SET YET, CHECK CONFIGURATION", <1,0,0>,1);
						}
						else
						{
							//If an invalid line, call an error and halt.
							llSetText("ERROR: INVALID CONFIGURATION\nLINE: " + (string)gNotecardLine, <1,0,0>,1);
						}
					}
					else
					{
						++gNotecardLine;
						gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
					}
					
				}
				else
				{
					++gNotecardLine;
					gNotecardQuery = llGetNotecardLine("Sounds.cfg", gNotecardLine);
				}
			}
		}
	}
}

state Trigger
{
	state_entry()
	{
		llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_ACTIVE|SOUNDS|" + gNodeName); //Let the engine know the module has activated successfully.
		llListen(gListenChannel, "",gGameEngineUUID, "");
	}
	
	listen(integer channel, string name, key id, string message)
	{
		message = llToUpper(message);
		if(message == "NEW_GAME")
		{
			llStopSound();
			state running;
		}
		else if(message == "SET_INACTIVE")
		{
			llStopSound();
			state inactive;
		}
		else
		{
			list tCommand = llParseString2List(message, ["|"], []);
			if(llList2String(tCommand, 0) == "PLAY")
			{
				string tRef = (string)llListFindList(tCommand, ["REF"]);
				float tVol = (float)llListFindList(tCommand, ["VOL"]);
				float tTim = (float)llListFindList(tCommand, ["TIM"]);
				integer tTyp = llListFindList(tCommand, ["TYP"]);
				
				if(tVol != -1)
				{
					tVol = llList2Float(tCommand, (integer)tVol);
				}
				else
				{
					tVol = 0.5;
				}
				if(tTim != -1)
				{
					tTim = llList2Float(tCommand, (integer)tTim);
				}
				else
				{
					tTim = gLoopTime;
				}
				if(tTyp != -1)
				{
					tTyp = llList2Integer(tCommand, tTyp);
				}
				else
				{
					tTyp = gType;
				}
				if(tRef != "-1")
				{
					tRef = llList2String(tCommand, (integer)tRef+1);
					wsPlaySound(tRef, tTyp, tTim, tVol);
				}
				else
				{
					llRegionSay(DEBUG_CHANNEL, "ERROR: REFERENCE NAME NOT GIVEN");
				}			
			}
			if(llList2String(tCommand, 0) == "STOP")
			{
				llStopSound();
				llSetTimerEvent(0);
			}
		}
	}
	
	timer()
	{
		llStopSound();
		llSetTimerEvent(0);
	}
}

state Ambient
{
	state_entry()
	{
		llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_ACTIVE|SOUNDS|" + gNodeName); //Let the engine know the module has activated successfully.
		llListen(gListenChannel, "",gGameEngineUUID, "");
		wsStartLoopedSounds();
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "NEW_GAME")
		{
			wsStopLoopedSounds();
			state running;
		}
		else if(message == "SET_INACTIVE")
		{
			wsStopLoopedSounds();
			state inactive;
		}
	}
	
	timer()
	{
		wsPlayLoopedSounds();
		wsAdjustTimer();
	}
}


state inactive
{
	state_entry()
	{
		llRegionSayTo(gGameEngineUUID, gListenChannel, "MODULE_INACTIVE|SOUNDS|" + gNodeName);
		llListen(gListenChannel, "",gGameEngineUUID, ""); //This state ONLY listens for Game Engine communications.
	}
	
	listen(integer channel, string name, key id, string message)
	{
		if(message == "NEW_GAME")
		{
			state running;
		}
		else if(message == "SET_ACTIVE")
		{
			if(gType)
			{
				state Trigger;
			}
			else if(!gType)
			{
				state Ambient;
			}
		}
	}
}
