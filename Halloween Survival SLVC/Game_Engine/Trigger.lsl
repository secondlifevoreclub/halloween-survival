// Game_Engine.Trigger.lslp 
// 2018-09-20 20:00:23 - LSLForge (0.1.9.3) generated
//Event Trigger System
//FUNCTION: Trigger special events.  This will just be a generic trigger.
//EXAMPLE: Triggering antagonist howl
//EXAMPLE: Triggering atmosphere change
//EXAMPLE: Triggering different scenery changes (Dead bodies appearing nearby/behind, more blood, rain effects, etc.


string gGameIdentification = "SLVC Horror Survival";
integer gListenChannel;
key gGameEngineUUID;
list gIgnore = [];
string gEventName;
list gTriggerResults;
list gEndTriggerResults;
integer gAllowNearby;
integer gEndTrigger;
float gDelay;
float gRange;
integer gSensorEnabled = 0;
integer gEventStarted = 0;
integer gProcessing = 0;
key gAgentTriggered = NULL_KEY;
list gConfigurationOutput;
integer gNotecardLine;
key gNotecardQuery;

wsPingHandler(string fIdentifier,string fModule,key fGameEngineID,integer fChannel){
  if (((fIdentifier == gGameIdentification) && 1)) {
    llRegionSayTo(fGameEngineID,fChannel,("MODULE_PING|RETURN|" + fModule));
    (gListenChannel = fChannel);
    (gGameEngineUUID = fGameEngineID);
  }
}

integer wsParseIntegerConfig(string fInput){
  if (((fInput == "TRUE") || (fInput == "1"))) {
    return 1;
  }
  else  if (((fInput == "FALSE") || (fInput == "0"))) {
    return 0;
  }
  else  {
    return -1;
  }
}

integer wsSetConfigurationSetting(list fConfig){
  string fTest = llToUpper(llList2String(fConfig,0));
  if ((fTest == "GEVENTNAME")) {
    (gEventName = llList2String(fConfig,1));
    if ((gEventName == "")) {
      return 0;
    }
    else  {
      return 1;
    }
  }
  else  if ((fTest == "GTRIGGERRESULTS")) {
    string tTriggerResults = llList2String(fConfig,1);
    (tTriggerResults = llDeleteSubString(tTriggerResults,0,llSubStringIndex(tTriggerResults,"[")));
    (tTriggerResults = llDeleteSubString(tTriggerResults,llSubStringIndex(tTriggerResults,"]"),-1));
    (gTriggerResults = llParseString2List(tTriggerResults,[","],[]));
    if ((gTriggerResults == [])) {
      return 0;
    }
    else  {
      return 1;
    }
  }
  else  if ((fTest == "GENDTRIGGERRESULTS")) {
    string tTriggerResults = llList2String(fConfig,1);
    (tTriggerResults = llDeleteSubString(tTriggerResults,0,llSubStringIndex(tTriggerResults,"[")));
    (tTriggerResults = llDeleteSubString(tTriggerResults,llSubStringIndex(tTriggerResults,"]"),-1));
    (gTriggerResults = llParseString2List(tTriggerResults,[","],[]));
    if ((gTriggerResults == [])) {
      return 0;
    }
    else  {
      return 1;
    }
  }
  else  if ((fTest == "GENDTRIGGER")) {
    (gEndTrigger = wsParseIntegerConfig(llList2String(fConfig,1)));
    if ((gEndTrigger == -1)) {
      return 0;
    }
    else  {
      return 1;
    }
  }
  else  if ((fTest == "GALLOWNEARBY")) {
    (gAllowNearby = wsParseIntegerConfig(llList2String(fConfig,1)));
    if ((gAllowNearby == -1)) {
      return 0;
    }
    else  {
      return 1;
    }
  }
  else  if ((fTest == "GDELAY")) {
    (gDelay = llList2Float(fConfig,1));
    return 1;
  }
  else  if ((fTest == "GRANGE")) {
    (gRange = llList2Float(fConfig,1));
    return 1;
  }
  return 0;
}


default {

    state_entry() {
    llListen(-7843275,"","","");
    llSetText("Listening for Game Engine",<1.0,1.0,1.0>,1);
  }

    
    listen(integer channel,string name,key id,string message) {
    if (((channel == -7843275) && (llListFindList(gIgnore,[id]) == -1))) {
      list tSystemCall = llParseString2List(message,["|"],[]);
      if ((llList2String(tSystemCall,0) == "MODULE_PING")) {
        wsPingHandler(llList2String(tSystemCall,1),"TRIGGER",id,llList2Integer(tSystemCall,2));
      }
      else  if ((message == "TURN_ON_MODULE")) {
        llSetText("Module Booting...",<1.0,1.0,1.0>,1);
        state boot;
      }
      else  if ((message == "IGNORE_PING_60")) {
        (gIgnore += [id,(llGetUnixTime() + 60)]);
        llSetTimerEvent(5);
      }
    }
  }

    
    timer() {
    if (llGetListLength(gIgnore)) {
      integer i;
      for ((i = llGetListLength(gIgnore)); (i > 0); (i -= 2)) {
        if ((llList2Integer(gIgnore,i) <= llGetUnixTime())) {
          (gIgnore = llDeleteSubList(gIgnore,(i - 1),i));
        }
      }
      if ((gIgnore == [])) {
        llSetTimerEvent(0);
      }
    }
  }
}

state boot {

	state_entry() {
    if ((llGetInventoryKey("Trigger.cfg") != NULL_KEY)) {
      (gConfigurationOutput = []);
      (gNotecardLine = 0);
      (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
    }
    else  {
      llSetText("ERROR: NO CONFIGURATION NOTECARD",<1.0,0.0,0.0>,1);
    }
  }

	
	dataserver(key query_id,string data) {
    if ((query_id == gNotecardQuery)) {
      if ((data == EOF)) {
        llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_LOADED|TRIGGER|" + gEventName));
      }
      else  {
        (data = llStringTrim(data,3));
        if ((data != "")) {
          if ((llSubStringIndex(data,"//") != 0)) {
            if ((llSubStringIndex(data,"//") != -1)) {
              (data = llDeleteSubString(data,llSubStringIndex(data,"//"),-1));
            }
            list tData = llParseString2List(data,[":","="],[]);
            integer i;
            for ((i = 0); (i < llGetListLength(tData)); (++i)) {
              (tData = llListReplaceList(tData,[llStringTrim(llList2String(tData,i),3)],i,i));
            }
            (i = wsSetConfigurationSetting(tData));
            if (i) {
              (++gNotecardLine);
              (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
            }
            else  {
              llSetText(("ERROR: INVALID CONFIGURATION\nLINE: " + ((string)gNotecardLine)),<1.0,0.0,0.0>,1);
            }
          }
          else  {
            (++gNotecardLine);
            (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
          }
        }
        else  {
          (++gNotecardLine);
          (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
        }
      }
    }
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "ACCEPT_MODULE")) {
      state running;
    }
    else  if ((message == "DUPLICATE_EVENT")) {
      llSetText("ERROR: DUPLICATE EVENT NAME",<1.0,0.0,0.0>,1);
    }
  }
}

state running {

	state_entry() {
    llListen(gListenChannel,"",gGameEngineUUID,"");
    llSetText("Awaitng Game to Start...",<1.0,1.0,1.0>,1);
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "START_GAME")) {
      llSetText("",<1.0,1.0,1.0>,1);
      state active;
    }
    else  if ((message == "RESET_GAME")) {
      llResetScript();
    }
    else  if ((message == "DUPLICATE_EVENT")) {
      llSetText("ERROR: DUPLICATE EVENT NAME",<1.0,0.0,0.0>,1);
    }
    else  if ((message == "RELOAD_CONFIGURATION")) {
      if ((llGetInventoryKey("Trigger.cfg") != NULL_KEY)) {
        llSetText("Loading Config...",<1.0,1.0,1.0>,1);
        (gConfigurationOutput = []);
        (gNotecardLine = 0);
        (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
      }
      else  {
        llSetText("ERROR: NO CONFIGURATION NOTECARD",<1.0,0.0,0.0>,1);
      }
    }
  }

	
		dataserver(key query_id,string data) {
    if ((query_id == gNotecardQuery)) {
      if ((data == EOF)) {
        llSetText("Awaitng Game to Start...",<1.0,1.0,1.0>,1);
        llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_LOADED|TRIGGER|" + gEventName));
      }
      else  {
        (data = llStringTrim(data,3));
        if ((data != "")) {
          if ((llSubStringIndex(data,"//") != 0)) {
            if ((llSubStringIndex(data,"//") != -1)) {
              (data = llDeleteSubString(data,llSubStringIndex(data,"//"),-1));
            }
            list tData = llParseString2List(data,[":","="],[]);
            integer i;
            for ((i = 0); (i < llGetListLength(tData)); (++i)) {
              (tData = llListReplaceList(tData,[llStringTrim(llList2String(tData,i),3)],i,i));
            }
            (i = wsSetConfigurationSetting(tData));
            if (i) {
              (++gNotecardLine);
              (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
            }
            else  {
              llSetText(("ERROR: INVALID CONFIGURATION\nLINE: " + ((string)gNotecardLine)),<1.0,0.0,0.0>,1);
            }
          }
          else  {
            (++gNotecardLine);
            (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
          }
        }
        else  {
          (++gNotecardLine);
          (gNotecardQuery = llGetNotecardLine("Trigger.cfg",gNotecardLine));
        }
      }
    }
  }
}

state active {

	state_entry() {
    llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_ACTIVE|TRIGGER|" + gEventName));
    llListen(gListenChannel,"",gGameEngineUUID,"");
    llVolumeDetect(1);
  }

	
	collision_start(integer num_detected) {
    if ((!gProcessing)) {
      (gProcessing = 1);
      (gAgentTriggered = llDetectedKey(0));
      if (((!gAllowNearby) && (!gEndTrigger))) {
        (gSensorEnabled = 1);
        llSensorRepeat("","",1,gRange,3.14159265,1);
      }
      else  if (gAllowNearby) {
        if ((gDelay == 0)) {
          llRegionSayTo(gGameEngineUUID,gListenChannel,((("EVENT_START|" + gEventName) + "|") + llDumpList2String(gTriggerResults,"|")));
        }
        else  {
          llSetTimerEvent(gDelay);
        }
      }
    }
  }

	
	collision_end(integer num_detected) {
    if ((gAgentTriggered == llDetectedKey(0))) {
      if (((!gEndTrigger) && gAllowNearby)) {
        if (gEventStarted) {
          llRegionSayTo(gGameEngineUUID,gListenChannel,("EVENT_END|" + gEventName));
          (gProcessing = 0);
          (gAgentTriggered = NULL_KEY);
        }
        else  if (gDelay) {
          llSetTimerEvent(0);
        }
      }
      else  if ((gEndTrigger && (!gAllowNearby))) {
        if (gSensorEnabled) {
          llSensorRemove();
        }
        llSensorRepeat("","",1,gRange,3.14159265,1);
      }
      else  {
        if ((gDelay == 0)) {
          llRegionSayTo(gGameEngineUUID,gListenChannel,((("EVENT_END|" + gEventName) + "|") + llDumpList2String(gEndTriggerResults,"|")));
          (gProcessing = 0);
          (gAgentTriggered = NULL_KEY);
        }
        else  {
          llSetTimerEvent(gDelay);
        }
      }
    }
  }

	
	sensor(integer num_detected) {
    if ((!num_detected)) {
      (gSensorEnabled = 0);
      if ((gDelay == 0)) {
        llRegionSayTo(gGameEngineUUID,gListenChannel,((("EVENT_START|" + gEventName) + "|") + llDumpList2String(gTriggerResults,"|")));
      }
      else  {
        llSetTimerEvent(gDelay);
      }
      llSensorRemove();
    }
  }

	
	timer() {
    llRegionSayTo(gGameEngineUUID,gListenChannel,((("EVENT_START|" + gEventName) + "|") + llDumpList2String(gTriggerResults,"|")));
    if (gAllowNearby) {
      (gEventStarted = 1);
    }
    llSetTimerEvent(0);
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "NEW_GAME")) {
      state running;
    }
    else  if ((message == "SET_INACTIVE")) {
      if ((gEventStarted && gEndTrigger)) {
        llRegionSayTo(gGameEngineUUID,gListenChannel,((("EVENT_END|" + gEventName) + "|") + llDumpList2String(gEndTriggerResults,"|")));
      }
      (gProcessing = 0);
      (gAgentTriggered = NULL_KEY);
      state inactive;
    }
  }
}

state inactive {

	state_entry() {
    llRegionSayTo(gGameEngineUUID,gListenChannel,("MODULE_INACTIVE|TRIGGER|" + gEventName));
    llListen(gListenChannel,"",gGameEngineUUID,"");
  }

	
	listen(integer channel,string name,key id,string message) {
    if ((message == "NEW_GAME")) {
      state running;
    }
    else  if ((message == "SET_ACTIVE")) {
      state active;
    }
  }
}
